package api;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2018-05-21T07:37:16")
@StaticMetamodel(Dosen.class)
public class Dosen_ { 

    public static volatile SingularAttribute<Dosen, Date> birthday;
    public static volatile SingularAttribute<Dosen, String> placeOfBirth;
    public static volatile SingularAttribute<Dosen, String> address;
    public static volatile SingularAttribute<Dosen, String> nip;
    public static volatile SingularAttribute<Dosen, String> phone;
    public static volatile SingularAttribute<Dosen, String> name;
    public static volatile SingularAttribute<Dosen, String> email;

}